Stringish
===============

[![Codecov](https://codecov.io/bb/java-universum/stringish/branch/main/graph/badge.svg)](https://codecov.io/bb/java-universum/stringish)
[![Codacy](https://api.codacy.com/project/badge/Grade/f55c3486051644e68968ecb8402839b1)](https://www.codacy.com/app/universum-studios/stringish?utm_source=java-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=java-universum/stringish&amp;utm_campaign=Badge_Grade)
[![Java](https://img.shields.io/badge/java-1.8-blue.svg)](https://java.com)

Java annotation processor which makes desired instances Stringish.

For more information please visit the **[Wiki](https://bitbucket.org/java-universum/stringish/wiki/Home)**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/java/universum.studios%3Astringish/images/download.svg)](https://bintray.com/universum-studios/java/universum.studios%3Astringish/_latestVersion)

Download the latest **[release](https://bitbucket.org/java-universum/stringish/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "universum.studios:stringish:${DESIRED_VERSION}"
    annotationProcessor "universum.studios:stringish-processor:${DESIRED_VERSION}"

## License ##

Licensed under the **Apache License**: **[Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)** or later.

> You can redistribute, modify or publish any part of the code presented within this repository but as it is described in the [**LICENSE**](https://bitbucket.org/java-universum/stringish/src/main/LICENSE.md), the software distributed under the License is distributed on an **"AS IS" BASIS, WITHOUT WARRANTIES or CONDITIONS OF ANY KIND**.
