/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.stringish.processor;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;

import universum.studios.stringish.Stringify;

/**
 * todo: description
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class StringishProcessor extends AbstractProcessor {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "StringishProcessor";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public synchronized void init(@NotNull final ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		// todo: perform initialization here ...
	}

	/**
	 */
	@Override public Set<String> getSupportedAnnotationTypes() {
		final Set<Class<? extends Annotation>> supportedAnnotations = getSupportedAnnotations();
		final Set<String> supportedTypes = new LinkedHashSet<>(supportedAnnotations.size());
		for (final Class<? extends Annotation> annotation : supportedAnnotations) {
			supportedTypes.add(annotation.getCanonicalName());
		}
		return supportedTypes;
	}

	/**
	 * Returns set of all annotations supported by this processor.
	 *
	 * @return Set with supported annotation classes.
	 */
	@NotNull private static Set<Class<? extends Annotation>> getSupportedAnnotations() {
		final Set<Class<? extends Annotation>> annotations = new LinkedHashSet<>(1);
		annotations.add(Stringify.class);
		return annotations;
	}

	/**
	 */
	@Override public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	/**
	 */
	@Override public boolean process(@NotNull final Set<? extends TypeElement> annotations, @NotNull final RoundEnvironment roundEnv) {
		// todo: implement processing here ...
		return false;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}